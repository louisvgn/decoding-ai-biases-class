from google.colab import drive
drive.mount('/content/drive')

# To get the maximum number of tokens that the responses need to be in order to reduce the cost of API usage
from mistral_common.tokens.tokenizers.mistral import MistralTokenizer
from mistral_common.protocol.instruct.tool_calls import Function, Tool, ToolCall, FunctionCall
from mistral_common.tokens.instruct.normalize import ChatCompletionRequest
from mistral_common.protocol.instruct.messages import (
    AssistantMessage,
    UserMessage,
    ToolMessage
)
tokenizer_v3 = MistralTokenizer.v3()
phrase = "10"
tokenized = tokenizer_v3.encode_chat_completion(
    ChatCompletionRequest(
        tools=[],
        messages=[UserMessage(content=phrase)],
        model="test",
    )
)

tokens, text = tokenized.tokens, tokenized.text
len(tokens)

# Actual implementation of the client API to query the model

from google.colab import userdata
from mistralai.client import MistralClient
from mistralai.models.chat_completion import ChatMessage
from mistralai.exceptions import MistralException
import time

api_key = userdata.get('mistral-key')

client = MistralClient(api_key=api_key)
model = "open-mixtral-8x22b"


def generate_response_api(template, question, max_tokens=5, max_retries=3, retry_delay=2):
    retries = 0
    while retries < max_retries:
        try:
            messages = [
                ChatMessage(role="system", content=template),
                ChatMessage(role="user", content=question)
            ]
            chat_response = client.chat(model=model, messages=messages, max_tokens=5)
            return chat_response.choices[0].message.content
        except MistralException as e:
            if "ReadTimeout" in str(e):
                print(f"#{retries}: Request timed out. Retrying...")
                time.sleep(retry_delay)
                retries += 1
            else:
                raise e  # Reraise if it's not a timeout issue
    raise TimeoutError("Exceeded maximum number of retries")


# Logic for importing templates and questions automatically from file

import csv
import json
import pandas as pd
import random

questions = pd.read_csv("questions.csv")
templates = pd.read_csv("templates.csv")
result = pd.merge(templates.assign(key=0), questions.assign(key=0), on='key').drop('key', axis=1)

def test_generate_response(template, question, id_set=None, id_question=None):
  response = random.randint(1, 10)
  return (response)

def save_checkpoint(result_rows, checkpoint_path):
    pd.DataFrame(result_rows).to_csv(checkpoint_path, index=False)

def load_checkpoint(checkpoint_path):
    if os.path.exists(checkpoint_path):
        try:
            return pd.read_csv(checkpoint_path)
        except pd.errors.EmptyDataError:
            print("Checkpoint file is empty. Returning None.")
            return None
    else:
        print("Checkpoint file not found. Returning None.")
        return None


def ask_question(df, checkpoint_path=None, max_tokens=5):
  grouped_profile = df.groupby('id_country')
  outputs = []
  result_rows = []
  iteration_count = 0 # checkpoint granularity for iterations instead of rows

  # Load checkpoint if available
  if checkpoint_path:
    checkpoint = load_checkpoint(checkpoint_path)
    if checkpoint is not None:
      result_rows = checkpoint.to_dict('records')

  # loop through each group of country
  for profile_id, group_profile_df in grouped_profile:
    #print(country)
    #print(group_profile_df)
    grouped_id_set = group_profile_df.groupby('id_set')

    # loop through each group of id_set
    for set_id, group_set_df in grouped_id_set:
      #print(set_id)

      # Get the corresponding txt_template based on id_set
      id_set_str = str(set_id)
      #print(id_set_str)

      template_column = f"txt_template_{id_set_str}"
      #print(template_column)

      template = group_set_df.iloc[0][template_column]
      #print(template)

      # shuffle questions
      shuffled_set = group_set_df.sample(frac=1)
      #print(shuffled_set)

      # Loop through each row in the group
      for index, row in shuffled_set.iterrows():

        # Get the txt_question
        id_country = row['id_country']
        id_question = row['id_question']
        id_set = row['id_set']
        question = row['txt_question']
        #print(id_set, id_question, question)

        output = generate_response_api(template=template, question=question, max_tokens=max_tokens)

        result_row = {
                    'id_country': id_country,
                    #'template': template_column,
                    'id_set': id_set,
                    'id_question': id_question,
                    #'txt_question': question,
                    'output': output
                }
        #print(result_row)

        result_rows.append(result_row)

        # Save checkpoint periodically
        iteration_count += 1
        if checkpoint_path and iteration_count % 10 == 0: # 10 means 10 iteration, change for more or less granularity
          save_checkpoint(result_rows, checkpoint_path)

        #print(iteration_count)

  if checkpoint_path:
    save_checkpoint(result_rows, checkpoint_path)

  result_df = pd.DataFrame(result_rows)
  return result_df


df_list = []
iteration_count = 0
for e in range(1):
  df = ask_question(result, "checkpoint")
  df['id_round'] = iteration_count
  df_list.append(df)
  iteration_count += 1
  print(f"Progress: Round number {iteration_count}…")


final_df = pd.concat(df_list)
final_df.sort_values(by=['id_country', 'id_round'], inplace=True)
final_df.reset_index(drop=True, inplace=True)

# changing id_round column index
id_round_index = final_df.columns.get_loc('id_round')
final_df.insert(1, 'id_round', final_df.pop('id_round'))
final_df.to_pickle('result_question.pkl')
