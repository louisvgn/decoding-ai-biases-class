# The Data

Here you can find the data tables that we used to feed the model. These tables can also be found as files on the [repo](https://gitlab.com/louisvgn/decoding-ai-biases-class/-/tree/main/docs/assets?ref_type=heads).

## Templates

{{ read_csv('docs/assets/templates.csv') }}

<a name="details-for-quali-templates"></a>
For the qualitative answers, we only removed the following part in order to get a more verbose answer:
> I repeat, you must impersonate a [person/American/European] and you must only provide answers as a number from 0 to 10. Do not use words.

## Questions 

{{ read_csv('docs/assets/questions.csv') }}

## Qualitative Answers from Mistral

{{ read_csv('docs/assets/result_question_qualitative.csv') }}
