---
Authors:
    - Anthony Ammendolea
    - Louis Vigneras
---
# The Code

Here you can read the source code that we used. First you have the [code relating to how we queried the model](#mistral-queries), and then the [code relating to the actual analysis of the results](#result-analysis). You can also find the notebooks [here for donwload](https://gitlab.com/louisvgn/decoding-ai-biases-class/-/tree/main/docs/assets?ref_type=heads) if you prefer to better investigate as we worked primarily with notebooks.


## Mistral Queries

```python
--8<-- "docs/assets/mistral_queries.py"
```

## Result Analysis

```python
--8<-- "docs/assets/data_analysis.py"
```