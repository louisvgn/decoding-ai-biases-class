---
Title: About
Authors: 
    - Louis Vigneras
toc: true
---
# About

This study was conducted as part of a class named *Decoding AI Biases* taught by Jean-Philippe Cointet (helped by Camille Chanial, the TA) at Sciences Po Paris in the *digital, new technology, and public policy* stream.

Anthony Ammendolea, Hamza Belgroun, Tom Foures, Nicolas Julian, and Louis Vigneras are the authors of this study. 

The instructions given were the following as *per* the syllabus:

> Identify/generate a dataset online, design an experimental plan to analyze its inherent biases, and finally visualize and reflect upon the systematic discriminations embedded in the dataset. The final delivery will take the form of a website.

## Diclaimer and Copyright

Unless otherwise stated, this content of this work (except source code) is licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1).

The source code is licensed under the [MIT license](https://gitlab.com/louisvgn/decoding-ai-biases-class/-/blob/main/LICENSE).

!!! info "Copyright"
     [Mistral "Le Chat" and European Biases](https://decoding-ai-biases-class-louisvgn-82dc9a5c0f11ecee82a5b8e8ee016.gitlab.io/) © 2024 by Anthony Ammendolea, Hamza Belgroun, Tom Foures, Nicolas Julian, Louis Vigneras is licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1)



This goes without saying, but in order to make sure there is no confusion, all the content present does not represent in any form endorsement by or the opinions of Sciences Po, the teachers, nor MISTRAL AI; they only represent the opinions and findings of the authors at the time of writing.


To render bibliographic ressources, we used the `mkdocs-bibtex` from [Shyam Dwaraknath](https://github.com/shyamd) and the CLS style files from the [CLS projet](https://citationstyles.org/).